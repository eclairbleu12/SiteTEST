from django.http import HttpResponse
from .models import Article, Comment
from forum.models import Category
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.template import loader
from django.utils import timezone
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

import random


def index(request):
    if request.method != "POST":
        latest_articles_list = Article.objects.order_by('-date_pub')[:5]
        template = loader.get_template('articles/index.html')
        context = {'latest_articles_list': latest_articles_list,}
        return render(request, 'articles/index.html', context)

    

def search(request):
    if request.method == 'GET': # If the form is submitted

            search_query = request.GET.get('search_box', None)
            search_query = search_query.lower()
            articles = Article.objects.all()
            categories = Category.objects.all()
            categories_pert = list()
            articles_pert = list()
            for article in articles:
                if search_query in article.contenu.lower() or search_query in article.titre.lower() or search_query in article.slug.lower() or search_query in article.autor.nom.lower() or search_query in article.autor.prenom.lower():
                    articles_pert.append(article)
            for category in categories:
                if search_query in category.name.lower():
                    categories_pert.append(category)
            for article in articles_pert:
                print(article.titre)
            context = {'articles': articles_pert,'cats': categories_pert,}
            return render(request, 'articles/search.html',context)


def read(request, article_slug):
    banniere = random.choice(settings.BANNIERES)
    article = get_object_or_404(Article, slug=article_slug)
    commentaires = Comment.objects.filter(article=article.pk)
    commentaires = commentaires[::-1]
    print(commentaires)
    fs = settings.USER_IMAGE_PATH
    bd = settings.MEDIA_URL
    print(str(fs)+'/'+str(article.autor.avatar))
    
    return render(request, 'articles/detail.html', {'article': article,'bd':bd,'commentaires':commentaires,'ban':banniere})


def vote(request, article_slug):

    article = get_object_or_404(Article, slug=article_slug)
    print('LLL : '+str(request.user.is_authenticated))
    #if str(request.user.is_authenticated) == 'True':
        # Redisplay the question voting form.
        #return 'Vous devez être connecté...'
    #else:
    
    comment = Comment(contenu=request.POST['contenu'],date_pub=timezone.now(),article=article,user=request.user)
    comment.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
    return HttpResponseRedirect(reverse('articles:read', args=(article_slug,)))