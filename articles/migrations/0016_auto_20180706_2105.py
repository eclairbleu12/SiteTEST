# Generated by Django 2.0.7 on 2018-07-06 21:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0015_auto_20180706_1257'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='slug',
            field=models.SlugField(max_length=255, unique=True),
        ),
        migrations.AlterField(
            model_name='comment',
            name='contenu',
            field=models.TextField(max_length=255),
        ),
    ]
