# Generated by Django 2.0.7 on 2018-07-06 12:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0014_article_vignette'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reporter',
            name='avatar',
            field=models.ImageField(upload_to='upload/vignettes/'),
        ),
    ]
