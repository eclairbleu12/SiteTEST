# Generated by Django 2.0.7 on 2018-07-04 20:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0004_auto_20180704_2011'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='slug',
            field=models.SlugField(max_length=500, unique=True),
        ),
    ]
