import datetime
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from slugify import slugify
from django.db import models
from django.utils import timezone
from tinymce.models import HTMLField
from django.contrib.auth import get_user_model
User = get_user_model()
from django import forms


# Create your models here.
class Reporter(models.Model):
    fs = FileSystemStorage(location=settings.USER_IMAGE_PATH)
    nom = models.CharField(max_length=20)
    prenom = models.CharField(max_length=20)
    avatar = models.ImageField(upload_to='upload/vignettes/')
    description = models.TextField(default="Hey ! Je suis un auteur !", null=True, blank=True)
    def __str__(self):
        return(self.prenom+' '+self.nom)
class Article(models.Model):

    titre = models.CharField(max_length=80)
    contenu = models.TextField()
    date_pub = models.DateTimeField('date published')
    slug = models.SlugField(max_length=255,unique=True)
    autor = models.ForeignKey(Reporter, on_delete=models.CASCADE)
    vignette = models.ImageField(upload_to='upload/')
    
    def __str__(self):
        return self.titre
class Comment(models.Model):
    contenu = models.TextField(max_length=255)
    date_pub = models.DateTimeField('Date de de publication')
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.article)+' : '+self.contenu

    


    

