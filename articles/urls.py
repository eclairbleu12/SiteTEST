from django.urls import path

from . import views
app_name = 'articles'
urlpatterns = [
    path('', views.index, name='index'),
    path('search',views.search,name="search"),
    path('<str:article_slug>/', views.read, name="read"),
    path('<str:article_slug>/vote', views.vote, name='vote'),
]