from django.contrib import admin
from .models import *


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('titre','date_pub','autor')
    list_filter = ['autor','date_pub']

admin.site.register(Article, ArticleAdmin)
admin.site.register(Reporter)
admin.site.register(Comment)

# Register your models here.
