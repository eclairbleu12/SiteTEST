# Generated by Django 2.0.7 on 2018-07-05 19:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='catégorie',
            name='forum',
        ),
        migrations.AddField(
            model_name='forum',
            name='catégories',
            field=models.ManyToManyField(to='forum.Catégorie'),
        ),
    ]
