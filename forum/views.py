from django.http import HttpResponse
from .models import *
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.template import loader
from django.utils import timezone
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator

# Create your views here.
def index(request):
    cats = Category.objects.all()
    
    lasted = list()
    for cat in cats:
        last = Post.objects.filter(category=cat)
        last = last[::-1]
        if last:
            lasted.append(last[0])
        else:
            lasted.append("...")
    
    mylist = zip(cats, lasted)
    context = {'cats':cats,'lasted':lasted,"zip":zip}
    template = loader.get_template('forum/index.html')
    return HttpResponse(template.render(context,request))
def category(request, category_slug):
    categorie = get_object_or_404(Category, slug=category_slug)
    posts = Post.objects.filter(category=categorie)
    print(posts)
    posts = posts[::-1]
    paginator = Paginator(posts, 2)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    context = {'posts':posts}
    template = loader.get_template('forum/category.html')
    return HttpResponse(template.render(context,request))
def post(request,category_slug,post_slug):
    categorie = get_object_or_404(Category, slug=category_slug)
    post = Post.objects.filter(slug=post_slug)
    print(post)
    context = {'post':post[0]}
    template = loader.get_template('forum/detail.html')
    return HttpResponse(template.render(context,request))