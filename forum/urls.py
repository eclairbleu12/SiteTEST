from django.urls import path

from . import views
app_name = 'forum'
urlpatterns = [
    path('', views.index,name="index"),
    path('<str:category_slug>/<str:post_slug>',views.post),
    path('<str:category_slug>', views.category),
]