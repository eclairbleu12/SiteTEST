from django.contrib import admin
from .models import *

admin.site.register(Gallery)
admin.site.register(Category)
admin.site.register(Photo)
# Register your models here.
