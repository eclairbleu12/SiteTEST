from django.db import models

# Create your models here.
class Gallery(models.Model):
    name = models.CharField(max_length=50)
    date_pub = models.DateField("Date de création")
    def __str__(self):
        return self.name
    slug = models.SlugField(max_length=255,unique=True)
class Category(models.Model):
    name = models.CharField(max_length=50)
    date_pub = models.DateField("Date création")
    slug = models.SlugField(max_length=255,unique=True)
class Photo(models.Model):
    name = models.CharField(max_length=50)
    date_pub = models.DateField(max_length=50)
    image = models.ImageField(upload_to='upload/gallery/')
    Gallery = models.ForeignKey(Gallery, on_delete=models.CASCADE)
    Category = models.ForeignKey(Category, on_delete=models.CASCADE)
    slug = models.SlugField(max_length=255,unique=True)