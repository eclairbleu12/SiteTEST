from django.shortcuts import render
from .models import Photo

# Create your views here.
def index(request):
    alls_photos = Photo.objects.all()
    context = {'photos': alls_photos,}
    return render(request, 'gallery/index.html',context)